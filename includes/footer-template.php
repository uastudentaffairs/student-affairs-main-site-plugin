<?php

// Make sure other sites can access
header("Access-Control-Allow-Origin: *");

// Set the plugin URL
$plugin_url = plugin_dir_url( dirname( __FILE__ ) );

// Set the images directory
$images_url = $plugin_url . 'images';

?><link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo $plugin_url; ?>css/sa-footer.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo $plugin_url; ?>css/sa-footer-print.min.css" type="text/css" media="print" />
<div id="sa-framework-footer-wrapper">

	<div id="sa-framework-footer">

		<div class="row">
			<div class="small-12 medium-4 columns sa-wordmark-column">
				<a class="fade" href="https://sa.ua.edu/">
					<img class="sa-wordmark" src="<?php echo $images_url; ?>/SA-wordmark-white.svg" alt=""/>
					<span class="screen-reader-text">The University of Alabama Division of Student Affairs</span>
				</a>
			</div>
			<div class="small-12 medium-4 medium-push-4 columns search-column"><?php

				// Define the search field title/placeholder text
				$search_placeholder_text = apply_filters( 'sa_framework_footer_search_placeholder_text', 'Search Student Affairs' );

				// Make sure we have text
				if ( ! $search_placeholder_text ) {
					$search_placeholder_text = 'Search Student Affairs';
				}

				?><div id="sa-framework-footer-search">
					<form role="search" method="get" class="search-form" action="https://sa.ua.edu/search/">
						<input type="hidden" name="cx" value="014059076202054157577:ozsjpjxzrhi">
						<input type="hidden" name="cof" value="FORID:10">
						<input type="hidden" name="ie" value="UTF-8">
						<label for="sa-framework-footer-search-field">
							<span class="screen-reader-text"><?php echo $search_placeholder_text; ?></span>
							<input id="sa-framework-footer-search-field" type="search" class="search-field" placeholder="<?php echo esc_attr( $search_placeholder_text  . '&hellip;' ); ?>" value="" name="s" title="<?php echo esc_attr( $search_placeholder_text ); ?>" />
						</label>
						<input id="sa-framework-footer-search-submit" type="submit" class="search-submit" value="<?php echo esc_attr( 'Search' ); ?>" />
						<div class="search-icon"></div> <!-- .search-icon -->
					</form>
				</div> <!-- #sa-framework-footer-search -->

			</div>
			<div class="small-12 medium-4 medium-pull-4 columns social-media-column">

				<span class="footer-header">Connect with Student Affairs</span>
				<ul id="sa-framework-footer-social-media">
					<li class="facebook">
						<a class="fade" href="https://www.facebook.com/UAStudentAffairs">
							<img class="icon" src="<?php echo $images_url; ?>/facebook-white.svg" alt="" />
							<span class="text screen-reader-text">Follow Student Affairs on Facebook</span>
						</a>
					</li>
					<li class="twitter">
						<a class="fade" href="https://twitter.com/UAStudents">
							<img class="icon" src="<?php echo $images_url; ?>/twitter-white.svg" alt="" />
							<span class="text screen-reader-text">Follow Student Affairs on Twitter</span>
						</a>
					</li>
					<li class="instagram">
						<a class="fade" href="https://instagram.com/uastudentaffairs/">
							<img class="icon" src="<?php echo $images_url; ?>/instagram-white.svg" alt="" />
							<span class="text screen-reader-text">Follow Student Affairs on Instagram</span>
						</a>
					</li>
				</ul> <!-- #sa-framework-footer-social-media -->

			</div>
		</div>

		<div class="footer-copyright">
			<div class="row">
				<div class="small-12 columns">
					<p>Copyright &copy; <?php echo date( 'Y' ); ?> <a href="http://ua.edu/">The University of Alabama</a> - <a href="https://sa.ua.edu/">Division of Student Affairs</a> <span class="hide-on-print">- <a href="http://www.ua.edu/disclaimer.html">Disclaimer</a> - <a href="http://www.ua.edu/privacy.html">Privacy</a> - <a href="https://sa.ua.edu/contactUs.cfm">Contact Us</a></span></p>
				</div>
			</div>
		</div>

	</div> <!-- #sa-framework-footer -->

</div> <!-- #sa-framework-footer-wrapper -->