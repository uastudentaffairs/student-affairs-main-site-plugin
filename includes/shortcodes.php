<?php

add_shortcode( 'print_sa_complaints_appeals', function( $args = array() ) {

	// Setup arguments
	$args = shortcode_atts( array(
		'complaintsform' => null,
	), $args, 'print_sa_complaints_appeals' );

	// Build the content
	$content = '<div id="sa-complaints">';

		$content .= '<div class="sa-complaint-choices">
			<div class="row">
				<div class="small-12 medium-6 columns">
					<div class="sa-complaint-choice show-complaints hide-complaint-choices"><span>I would like to submit a complaint</span></div>
				</div>
				<div class="small-12 medium-6 columns">
					<div class="sa-complaint-choice show-appeals hide-complaint-choices"><span>I would like to submit an appeal</span></div>
				</div>
			</div>
		</div>';

		$content .= '<ul id="sa-complaints-tabs" class="tabs" data-tab role="tablist">
			  <li class="tab-title active" role="presentation"><a href="#complaints" id="complaints-tab" role="tab" tabindex="0" aria-selected="true" aria-controls="complaints"">Submit A Complaint</a></li>
			  <li class="tab-title" role="presentation"><a href="#appeals" id="appeals-tab" role="tab" tabindex="0" aria-selected="false" aria-controls="appeals">Submit An Appeal</a></li>
			</ul>
			<div id="sa-complaints-content" class="tabs-content">
				<section role="complaints" aria-hidden="false" class="content active" id="complaints">
				    <h2 class="screen-reader-text">Submit A Complaint</h2>' . do_shortcode( '[gravityform id="' . $args[ 'complaintsform' ] . '" title="false" description="false" ajax="true"]' )
		      . '</section>
		        <section role="appeals" aria-hidden="true" class="content" id="appeals">
				    <h2 class="screen-reader-text">Submit An Appeal</h2>
					<p>Below is a list of topics of student concern. Choose one of the following for information on the Appeals process related to the topic:</p>
					<ul>
						<li><a href="http://graduate.ua.edu/catalog/13950.html" target="_blank">Academic Appeals: Graduate </a></li>
						<li><a href="http://courseleaf.ua.edu/introduction/academicpolicies/codeacademicconduct/#academicmisconducttext" target="_blank">Academic Appeals: Undergraduate </a></li>
						<li><a href="http://courseleaf.ua.edu/introduction/academicpolicies/codeacademicconduct/#academicmisconducttext" target="_blank">Academic Misconduct</a></li>
						<li><a href="http://courseleaf.ua.edu/introduction/academicpolicies/academicwarningacademicsuspension/#reinstatementprocedurestext" target="_blank">Academic Reinstatement</a></li>
						<li><a href="http://courseleaf.ua.edu/introduction/academicpolicies/academicwarningacademicsuspension/" target="_blank">Academic Warning and Academic Suspension</a></li>
						<li><a href="http://financialaid.ua.edu/other/appeal.html" target="_blank">Financial Aid Appeals</a></li>
						<li><a href="http://housing.ua.edu/new_students/freshman_residency.cfm" target="_blank">Freshman Residency Exemption</a></li>
						<li><a href="https://www.lib.ua.edu/using-the-library/circulation-services/fines/" target="_blank">Library Fines</a></li>
						<li><a href="http://bamadining.ua.edu/freshman-dining-program/" target="_blank">Meal Plan Exemption</a></li>
						<li><a href="http://bamaparking.ua.edu/regulations/#Appeals " target="_blank">Parking Appeals</a></li>
						<li><a href="http://courseleaf.ua.edu/introduction/codestudentconduct/v/" target="_blank">Student Judicial Appeals</a></li>
					</ul>
			  </section>
			</div>';

		$content .= '';

	$content .= '</div>';

	return $content;
});