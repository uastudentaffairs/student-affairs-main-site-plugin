<?php

class Student_Affairs_Main_Site_API {

	/**
	 * Warming things up.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function __construct() {

		// Register any REST fields
		add_action( 'rest_api_init', array( $this, 'register_rest_fields' ) );

	}

	/**
	 * Register additional fields for the REST API.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function register_rest_fields() {

		// Set default args
		$field_args = array(
			'get_callback'    => array( $this, 'get_field_value' ),
			'update_callback' => null,
			'schema'          => null,
		);

		// Register inclusion fields
		$inclusion_fields = array( 'website', 'office', 'phone', 'phone_tty' );
		foreach( $inclusion_fields as $field ) {
			register_rest_field( 'inclusion', $field, $field_args );
		}

		// Register department fields
		$department_fields = array( 'grid_photo', 'website', 'office', 'phone', 'twitter_handle', 'facebook_url', 'instagram_handle' );
		foreach( $department_fields as $field ) {
			register_rest_field( 'departments', $field, $field_args );
		}

	}

	/**
	 * Get field values for the REST API.
	 *
	 * @param	array - $object - details of current post
	 * @param	string - $field_name - name of field
	 * @param	WP_REST_Request - $request - current request
	 * @return	mixed
	 */
	public function get_field_value( $object, $field_name, $request ) {

		// Set post ID
		$post_id = $object[ 'id' ];

		switch( $field_name ) {

			case 'grid_photo':
				if ( ( $grid_photo_id = get_post_meta( $post_id, 'archive_grid_photo', true ) )
					&& ( $grid_photo_src = wp_get_attachment_image_src( $grid_photo_id, 'sa-archive-grid' ) ) ) {
					return $grid_photo_src[0];
				}
				break;

			case 'website':
			case 'office':
			case 'phone':
			case 'phone_tty':
			case 'twitter_handle':
			case 'facebook_url':
			case 'instagram_handle':

				// Do we have a department contact ID?
				if ( ( $department_contact_id = get_post_meta( $post_id, 'department_contact_id', true ) )
				     && $department_contact_id > 0 ) {
					$post_id = $department_contact_id;
				}

				// Add post meta
				if ( $meta_value = get_post_meta( $post_id, $field_name, true ) ) {
					return $meta_value;
				}

				break;

		}

		return '';
	}

}

// Let's get this show on the road
new Student_Affairs_Main_Site_API;