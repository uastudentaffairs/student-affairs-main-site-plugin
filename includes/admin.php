<?php

// Setup admin columns
add_filter( 'manage_posts_columns', function( $posts_columns, $post_type ) {

	// Set the columns we want to add after the title
	$add_columns_after_title = array();

	// Should we remove the title column?
	$remove_title_column = false;

	switch( $post_type ) {

		case 'otob':
			$add_columns_after_title[ 'post_content' ] = 'Message';
			$remove_title_column = true;
			break;

	}

	// If there's no title column, add our columns to the end
	if ( ! array_key_exists( 'title', $posts_columns ) ) {
		return array_merge( $posts_columns, $add_columns_after_title );
	}

	// Add our columns after the title column
	else {

		// Create a new set of columns
		$new_columns = array();

		foreach ( $posts_columns as $key => $label ) {

			// Add our columns after the title
			if ( 'title' == $key ) {

				// Add the title column if need be
				if ( ! $remove_title_column ) {
					$new_columns[ $key ] = $label;
				}

				$new_columns = array_merge( $new_columns, $add_columns_after_title );

			} else {

				// Add the other columns
				$new_columns[ $key ] = $label;

			}

		}

		return $new_columns;

	}

}, 100000000000, 2 );

// Populate the columns
add_action( 'manage_posts_custom_column', function( $column_name, $post_id ) {

	switch( $column_name ) {

		case 'post_content':

			if ( $value =  get_post_field( $column_name, $post_id ) ) {
				echo $value;
			}
			break;

	}

}, 100, 2 );

// Add custom field groups
add_action( 'admin_menu', function() {

	if ( function_exists( 'acf_add_local_field_group' ) ):

		acf_add_local_field_group( array(
			'key'                       => 'group_5602f557b9f05',
			'title'                     => 'Contact Information',
			'fields'                    => array(
				array(
					'key'               => 'field_5602f5d4957f4',
					'label'             => 'Website',
					'name'              => 'website',
					'type'              => 'url',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
				),
				array (
					'key' => 'field_563a1cb25597b',
					'label' => 'Main Office',
					'name' => 'office',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array(
					'key'               => 'field_5602f60c957f5',
					'label'             => 'Main Phone',
					'name'              => 'phone',
					'type'              => 'text',
					'instructions'      => '(XXX) XXX-XXXX',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
					'readonly'          => 0,
					'disabled'          => 0,
				),
				array(
					'key'               => 'field_5602f6367cb27',
					'label'             => 'Main Fax',
					'name'              => 'fax',
					'type'              => 'text',
					'instructions'      => '(XXX) XXX-XXXX',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
					'readonly'          => 0,
					'disabled'          => 0,
				),
				array (
					'key' => 'field_5664a7f8c9997',
					'label' => 'Main TTY',
					'name' => 'phone_tty',
					'type' => 'text',
					'instructions' => '(XXX) XXX-XXXX',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'departments',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'inclusion',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		) );

	endif;

});