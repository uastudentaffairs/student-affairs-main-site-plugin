<?php

// Filter the <body> class
add_filter( 'body_class', function( $classes, $class ) {

	// Let our styles know its the main site
	$classes[] = 'sa-main-site';

	return $classes;
}, 10, 2 );

// Filter the framework banner class
add_filter( 'sa_framework_banner_wrapper_classes', function( $classes ) {

	// Use a red banner for the main site
	$classes[] = 'red-banner';

	return $classes;
});

// Filter the banner breadcrumbs
add_filter( 'sa_framework_banner_breadcrumbs', function( $breadcrumbs ) {

	// Remove Student Affairs breadcrumb
	unset( $breadcrumbs[ 'sa' ] );

	return $breadcrumbs;
});

// Filter the page section post ID
add_filter( 'sa_framework_page_section_post_id', function( $page_section_post_id ) {

	if ( is_singular( 'inclusion' ) && ( $inclusion_page = get_page_by_path( 'diversity-and-inclusion/' ) ) ) {
		return $inclusion_page->ID;
	}

	return $page_section_post_id;
});

// Manage some filters
add_action( 'pre_get_posts', function( $query ) {

	// Don't trim the excerpt for the following post types
	// Have to place here so it will trigger for the API request
	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );

	// Remove the excerpt filter for these post types
	$remove_post_types = array( 'departments', 'inclusion' );
	if ( ( is_string( $post_type ) && in_array(  $post_type, $remove_post_types ) )
	     || ( is_array( $post_type ) && count( $post_type ) == 1 && array_intersect( $post_type, $remove_post_types ) ) ) {

		remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );

	}

	// Always order departments by name ASC
	// Have to check single array with json queries
	if ( 'departments' == $post_type
	     || ( is_array( $post_type ) && in_array( 'departments', $post_type ) && count( $post_type ) == 1 ) ) {

		$query->set( 'orderby' , 'title' );
		$query->set( 'order' , 'ASC' );

	}

});

// Set term link to department website
add_filter( 'term_link', function( $term_link, $term, $taxonomy ) {

	// Only for departments
	if ( 'departments' != $term->taxonomy ) {
		return $term_link;
	}

	return get_permalink( $term->term_id );
}, 10, 3 );

// Set permalink to department website
add_filter( 'post_type_link', function( $post_link, $post, $leavename, $sample ) {

	if ( 'departments' == $post->post_type ) {
		if ( $website = get_post_meta( $post->ID, 'website', true ) ) {
			return $website;
		}
		return null;
	}

	return $post_link;
}, 10, 4 );

// Tweak any department searches
add_filter( 'posts_search', function( $search, $query ) {
	global $wpdb;

	// Only for search
	if ( ! $query->is_search() ) {
		return $search;
	}

	// Only if we have search terms
	if ( ! isset( $query->query_vars['search_terms'] ) ) {
		return $search;
	}

	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );
	if ( 'departments' == $post_type
	     || ( is_array( $post_type ) && in_array( 'departments', $post_type ) && count( $post_type ) == 1 ) ) {

		// Make sure its an array
		if ( ! is_array( $query->query_vars['search_terms'] ) ) {
			$query->query_vars['search_terms'] = explode( ' ', $query->query_vars['search_terms'] );
		}

		// Remove the beginning 'AND (
		$search = preg_replace( '/^AND\s\(\(/i', '', ltrim( $search ) );

		$search_term_index = 0;
		foreach( $query->query_vars['search_terms'] as $search_term ) {
			$pre_search = " ( ( {$wpdb->posts}.post_excerpt LIKE '%{$search_term}%' ) OR EXISTS ( SELECT meta_value FROM {$wpdb->postmeta} WHERE post_id = {$wpdb->posts}.ID AND meta_key LIKE 'search_keywords_%_search_keyword' AND meta_value LIKE '%{$search_term}%' ) ";
			if ( $search_term_index < count( $search_term ) ) {
				$pre_search .= " OR ";
			}
			$search = $pre_search . $search;
			$search_term_index++;
		}

		// Add the parentheses back
		$search = ' AND (' . $search;

	}

	return $search;
}, 100, 2 );