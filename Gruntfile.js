module.exports = function(grunt) {

    grunt.initConfig({
        sass: {
            studentaffairsmain: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed',
                    noCache: true,
                    update: false
                },
                files: [{
                    expand: true,
                    src: '*.scss',
                    cwd: 'scss',
                    dest: 'css',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                mangle: false,
                compress: false
            },
            studentaffairsmain: {
                files: [{
                    expand: true,
                    src: [ '**/*.js', '!foundation.*.js', '!*.min.js' ],
                    cwd: 'js',
                    dest: 'js',
                    ext: '.min.js'
                }]
            }
        },
        watch: {
            studentaffairsmainjs: {
                files: [ 'js/*.js', '!foundation.*.js', '!*.min.js' ],
                tasks: [ 'newer:uglify:studentaffairsmain' ]
            },
            studentaffairsmainsass: {
                files: [ '**/*' ],
                tasks: [ 'newer:sass:studentaffairsmain' ]
            }
        }
    });

    // Load our dependencies
    grunt.loadNpmTasks( 'grunt-contrib-sass' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-watch' );
    grunt.loadNpmTasks( 'grunt-newer' );

    // Register our tasks
    grunt.registerTask( 'default', [ 'newer:sass', 'newer:uglify', 'watch' ] );

    // Register a watch function
    grunt.event.on( 'watch', function( action, filepath, target ) {
        grunt.log.writeln( target + ': ' + filepath + ' has ' + action );
    });

};