(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // Initialize Foundation
        $(document).foundation();

        $( '.hide-complaint-choices' ).on( 'click', function( $event ) {
            $event.preventDefault();
            $( '.sa-complaint-choices' ).hide();

        });

        $( '.show-complaints' ).on( 'click', function( $event ) {
            $event.preventDefault();
            $( '#complaints-tab' ).trigger( "click" );
            $( '#sa-complaints-tabs, #sa-complaints-content' ).show();
        });

        $( '.show-appeals' ).on( 'click', function( $event ) {
            $event.preventDefault();
            $( '#appeals-tab' ).trigger( "click" );
            $( '#sa-complaints-tabs, #sa-complaints-content' ).show();
        });

    });

})( jQuery );