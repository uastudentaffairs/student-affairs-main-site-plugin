<?php

/**
 * Plugin Name:       Student Affairs - The Main Site
 * Plugin URI:        https://sa.ua.edu
 * Description:       This WordPress plugin is intended solely for The University of Alabama Division of Student Affairs.
 * Version:           1.0
 * Author:            UA Division of Student Affairs - Rachel Carden
 * Author URI:        https://sa.ua.edu
 */

// Store that this is the main site
define( 'IS_SA_MAIN_SITE', true );
wp_cache_set( 'is_main_site', true, 'sa_framework' );

// Load the files
require_once plugin_dir_path( __FILE__ ) . 'includes/filters.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/shortcodes.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/api.php';

// We only need you in the admin
if ( is_admin() ) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/admin.php';
}

// Runs on install
register_activation_hook( __FILE__, 'ua_sa_main_site_install' );
function ua_sa_main_site_install() {

	// Register the custom post types
	ua_sa_main_site_register_cpts();

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Runs when the plugin is upgraded
// @TODO test to see if this only runs when running a bulk upgrade
add_action( 'upgrader_process_complete', 'ua_sa_main_site_upgrader_process_complete', 1, 2 );
function ua_sa_main_site_upgrader_process_complete( $upgrader, $upgrade_info ) {

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Setup the global footer template to be shown at {site}/footer
add_action( 'plugins_loaded', function() {
	if ( preg_match( '/^\/?footer\/?$/i', $_SERVER[ 'REQUEST_URI' ] ) ) {
		require_once plugin_dir_path( __FILE__ ) . 'includes/footer-template.php';
		exit;
	}
});

// Setup styles and scripts
add_action( 'wp_enqueue_scripts', function () {
	global $post;

	// Get the plugin path
	$plugin_dir = plugin_dir_url( __FILE__ );

	// Enqueue main site styles
	wp_enqueue_style( 'sa-main-site', $plugin_dir . 'css/sa-main-site.min.css', array( 'sa-child' ) );

	// Enqueue complaints script
	if ( isset( $post->post_content ) && has_shortcode( $post->post_content, 'print_sa_complaints_appeals' ) ) {
		wp_register_script( 'foundation', $plugin_dir . 'js/foundation.min.js', array( 'jquery' ) );
		wp_register_script( 'foundation-tab', $plugin_dir . 'js/foundation.tab.min.js', array( 'foundation' ) );
		wp_enqueue_script( 'sa-complaints', $plugin_dir . 'js/sa-complaints.min.js', array( 'jquery', 'foundation-tab' ) );
	}

}, 30 );

// Register custom post type
add_action( 'init', 'ua_sa_main_site_register_cpts' );
function ua_sa_main_site_register_cpts() {

	// Register departments CPT
	register_post_type( 'departments', array(
		'labels' => array(
			'name'               => _x( 'Departments', 'post type general name', 'ua-sa-departments' ),
			'singular_name'      => _x( 'Department', 'post type singular name', 'ua-sa-departments' ),
			'menu_name'          => _x( 'Departments', 'admin menu', 'ua-sa-departments' ),
			'name_admin_bar'     => _x( 'Departments', 'add new on admin bar', 'ua-sa-departments' ),
			'add_new'            => _x( 'Add New', 'departments', 'ua-sa-departments' ),
			'add_new_item'       => __( 'Add New Department', 'ua-sa-departments' ),
			'new_item'           => __( 'New Department', 'ua-sa-departments' ),
			'edit_item'          => __( 'Edit Department', 'ua-sa-departments' ),
			'view_item'          => __( 'View Department', 'ua-sa-departments' ),
			'all_items'          => __( 'All Departments', 'ua-sa-departments' ),
			'search_items'       => __( 'Search Departments', 'ua-sa-departments' ),
			'parent_item_colon'  => __( 'Parent Department:', 'ua-sa-departments' ),
			'not_found'          => __( 'No departments found.', 'ua-sa-departments' ),
			'not_found_in_trash' => __( 'No departments found in Trash.', 'ua-sa-departments' )
		),
		'public'                => true,
		'publicly_queryable'    => true,
		'exclude_from_search'   => false,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-networking',
		'capabilities'          => array(
			'edit_post'         => 'edit_department',
			'read_post'         => 'read_department',
			'delete_post'       => 'delete_department',
			'edit_posts'        => 'edit_departments',
			'edit_others_posts' => 'edit_others_departments',
			'publish_posts'     => 'publish_departments',
			'read_private_posts'=> 'read_private_departments',
			'read'              => 'read',
			'delete_posts'      => 'delete_departments',
			'delete_private_posts' => 'delete_private_departments',
			'delete_published_posts' => 'delete_published_departments',
			'delete_others_posts' => 'delete_others_departments',
			'edit_private_posts' => 'edit_private_departments',
			'edit_published_posts' => 'edit_published_departments',
			'create_posts'      => 'edit_departments'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array(
			'slug'  => 'departments',
			'feeds' => false,
			'pages' => true,
		),
		'query_var'             => true,
		'show_in_rest'          => true,
	) );

	// Register inclusion CPT
	register_post_type( 'inclusion', array(
		'labels' => array(
			'name'               => _x( 'Inclusion', 'post type general name', 'uastudentaffairs' ),
			'singular_name'      => _x( 'Inclusion', 'post type singular name', 'uastudentaffairs' ),
			'menu_name'          => _x( 'Inclusion', 'admin menu', 'uastudentaffairs' ),
			'name_admin_bar'     => _x( 'Inclusion', 'add new on admin bar', 'uastudentaffairs' ),
			'add_new'            => _x( 'Add New', 'inclusion', 'uastudentaffairs' ),
			'add_new_item'       => __( 'Add New Inclusion', 'uastudentaffairs' ),
			'new_item'           => __( 'New Inclusion', 'uastudentaffairs' ),
			'edit_item'          => __( 'Edit Inclusion', 'uastudentaffairs' ),
			'view_item'          => __( 'View Inclusion', 'uastudentaffairs' ),
			'all_items'          => __( 'All Inclusion', 'uastudentaffairs' ),
			'search_items'       => __( 'Search Inclusion', 'uastudentaffairs' ),
			'parent_item_colon'  => __( 'Parent Inclusion:', 'uastudentaffairs' ),
			'not_found'          => __( 'No inclusion found.', 'uastudentaffairs' ),
			'not_found_in_trash' => __( 'No inclusion found in Trash.', 'uastudentaffairs' )
		),
		'public'                => true,
		'publicly_queryable'    => true,
		'exclude_from_search'   => false,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-universal-access-alt',
		'capabilities'          => array(
			'edit_post'         => 'edit_inclusion',
			'read_post'         => 'read_inclusion',
			'delete_post'       => 'delete_inclusion',
			'edit_posts'        => 'edit_inclusions',
			'edit_others_posts' => 'edit_others_inclusions',
			'publish_posts'     => 'publish_inclusions',
			'read_private_posts'=> 'read_private_inclusions',
			'read'              => 'read',
			'delete_posts'      => 'delete_inclusions',
			'delete_private_posts' => 'delete_private_inclusions',
			'delete_published_posts' => 'delete_published_inclusions',
			'delete_others_posts' => 'delete_others_inclusions',
			'edit_private_posts' => 'edit_private_inclusions',
			'edit_published_posts' => 'edit_published_inclusions',
			'create_posts'      => 'edit_inclusions'
		),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array( 'slug' => '/diversity-and-inclusion'),
		'query_var'             => true,
	) );

}